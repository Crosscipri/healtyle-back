import { Schema , model } from 'mongoose';

const participantsIconsSchema = new Schema({
    participantId: {
        type: String
    },
    name: {
        type: String
    },
    unlocked: {
        type: Boolean
    },
    title: {
        type: String
    },
    stars: {
        type: Number
    }
});

export default model('ParticipantsIcons', participantsIconsSchema);

