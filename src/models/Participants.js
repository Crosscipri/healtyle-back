import { Schema , model } from 'mongoose';

const participantsSchema = new Schema({
    userId: {
        type: String
    },
    participant: {
        type: String,
        trim: true,
    },
    isSelect: {
        type: Boolean
    },
    name: {
        type: String,
        trim: true,
    },
    surname: {
        type: String,
        trim: true,
    },
    secondSurname: {
        type: String,
        trim: true,
    },
    genre: {
        type: String,
        trim: true,
    },
    weight: [{
        weight: Number,
        date: Date
    }],
    height: [{
        height: Number,
        date: Date
    }],
    bornDate: {
        type: Date,
    },
    weightTarget: {
        type: Object
    },
    totalStars: {
        type: Number
    },
    icon: {
        type: String,
    },
    activities: [{
        ref: "activities",
        type: Schema.Types.ObjectId
    }]
}, {
    timestamps: true
});

export default model('Participants', participantsSchema);

