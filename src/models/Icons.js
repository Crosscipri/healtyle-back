import { Schema , model } from 'mongoose';

const iconsSchema = new Schema({
    name: {
        type: String
    },
    unlocked: {
        type: Boolean
    },
    title: {
        type: String
    },
    stars: {
        type: Number
    }

});

export default model('icons', iconsSchema, 'icons');

