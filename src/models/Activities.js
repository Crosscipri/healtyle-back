import { Schema , model } from 'mongoose';

const activitiesSchema = new Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    type: {
        type: String
    },
    objective: {
        type: String
    },
    icon: {
        type: String
    },
    difficulty: {
        type: String
    },
    reward: {
        type: Number
    },
    unlockedStars: {
        type: Number
    },
    unlocked: {
        type: Boolean
    },
    initDate: {
        type: Date
    },

}, {
    timestamps: true
});

export default model('activities', activitiesSchema, 'activities');

