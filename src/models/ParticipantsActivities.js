import { Schema , model } from 'mongoose';

const participantsActivitiesSchema = new Schema({
    participantId: {
        type: String
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    type: {
        type: String
    },
    objective: {
        type: String
    },
    icon: {
        type: String
    },
    difficulty: {
        type: String
    },
    reward: {
        type: Number
    },
    unlockedStars: {
        type: Number
    },
    unlocked: {
        type: Boolean
    },
    initDate: {
        type: Date
    },
});

export default model('ParticipantsActivities', participantsActivitiesSchema);

