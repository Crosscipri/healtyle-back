import { Schema , model } from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const userSchema = new Schema({
    username: {
        type: String,
        required: true,
        trim: true,
        unique: false
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    confirmPassword: {
        type: String,
        required: false
    },
    hasTermsAndConditions: {
        type: Boolean,
        required: true
    },
    participants: [{
        ref: "Participants",
        type: Schema.Types.ObjectId
    }]
}, {
    timestamps: true
});

userSchema.statics.encryptPassword = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
 };
 
 userSchema.statics.comparePassword = (receivedPassword, password) => {
    return bcrypt.compareSync(receivedPassword, password);
 }

 
export default model('User', userSchema);