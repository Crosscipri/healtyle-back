import Participants from '../models/Participants';
import ParticipantsActivities from '../models/ParticipantsActivities';
import ParticipantsIcons from '../models/ParticipantsIcons';
import User from '../models/User';
import Activities from '../models/Activities';
import Icons from '../models/Icons';
import { getId } from '../middlewares';

export const postParticipant = async (req, res) => {
    const userId = getId(req)
    const {
        newUser,
    } = req.body;

    const user = await User.findById(userId, { password: 0, confirmPassword: 0 });
    if (!user) return res.status(404).json({ message: "No user found" })

    const activities = await Activities.find();
    if (!activities) return res.status(404).json({ message: "No activities found" });

    const icons = await Icons.find();
    if (!icons) return res.status(404).json({ message: "No icons found" });

    const participants = await Participants.find({ userId });
    if (!participants) return res.status(404).json({ message: "No participants found" })

    let newParticipant = new Participants({
        userId: userId,
        participant: newUser,
        totalStars: 0,
        isSelect: false,
        icon: 'default'
    });

    if (participants.length === 0) {
        newParticipant = new Participants({
            userId: userId,
            participant: newUser,
            totalStars: 0,
            isSelect: true,
            icon: 'default'
        });
    }

    user.participants.push(newParticipant);
    await user.save();

    await newParticipant.save();
  
    activities.map(activitie => {
        let newActivitie = new ParticipantsActivities({
            participantId: newParticipant._id,
            title: activitie.title,
            description: activitie.description,
            type: activitie.type,
            objective: activitie.objective,
            icon: activitie.icon,
            difficulty: activitie.difficulty,
            reward: activitie.reward,
            duration: activitie.duration,
            unlockedStars: activitie.unlockedStars,
            inProgress: activitie.inProgress,
            unlocked: activitie.unlocked
        });
        
        newActivitie.save()
    })

    icons.map(icons => {
        let newIcons = new ParticipantsIcons({
            participantId: newParticipant._id,
            name: icons.name,
            unlocked: icons.unlocked,
            title: icons.title,
            stars: icons.stars
        });
        
        newIcons.save()
    })

    res.status(200).json({ newParticipant })

};

export const getParticipants = async (req, res) => {
    const { userId } = req.params;

    const participants = await Participants.find({ userId }).populate("activities");;
    if (!participants) return res.status(404).json({ message: "No participants found" })

    res.status(200).json({ participants })
}

export const getParticipantById = async (req, res) => {
    const { participantId } = req.params;

    const participant = await Participants.findById({ _id: participantId }).populate("activities");;
    if (!participant) return res.status(404).json({ message: "No participant found" })

    res.status(200).json({ participant })
}

export const putSelectParticipant = async (req, res) => {
    const { selectedId } = req.body;

    const participants = await Participants.findOneAndUpdate({ isSelect: true }, { isSelect: false });
    if (!participants) return res.status(404).json({ message: "No participants found" })

    const selectedParticipant = await Participants.findByIdAndUpdate({ _id: selectedId }, { isSelect: true });
    if (!selectedParticipant) return res.status(404).json({ message: "No participant found" })


    res.status(200).json({ message: " Update succesfull" });

}

export const putParticipant = async (req, res) => {

    const { participantId } = req.body
    const { name, surname, secondSurname, genre, height, weight, bornDate } = req.body.data;

    const floatHeight = parseFloat(height);
    const floatWeight = parseFloat(weight);

    const selectedParticipant = await Participants.findByIdAndUpdate({ _id: participantId }, {
        name: name,
        surname: surname,
        secondSurname: secondSurname,
        genre: genre,
        bornDate: bornDate,
    });

    if(floatHeight) {
        await Participants.findByIdAndUpdate({ _id: participantId }, {
             $push: { weight: { weight: floatWeight, date: Date.now() } },
        });
        await Participants.findByIdAndUpdate({ _id: participantId }, {
            $push: { height: { height: floatHeight, date: Date.now() } },
        });
    }
    if (!selectedParticipant) return res.status(404).json({ message: "No participant found" })

    res.status(200).json({ message: " Update succesfull" });
}

export const putProfileDataParticipant = async (req, res) => {

    const { participantId } = req.body
    const { height, weight } = req.body.data;

    const floatHeight = parseFloat(height);
    const floatWeight = parseFloat(weight);

    const selectedParticipant = await Participants.findByIdAndUpdate({ _id: participantId }, {
        $push: { weight: { weight: floatWeight, date: Date.now() } },
    });
    await Participants.findByIdAndUpdate({ _id: participantId }, {
        $push: { height: { height: floatHeight, date: Date.now() } },
    });
    if (!selectedParticipant) return res.status(404).json({ message: "No participant found" })

    res.status(200).json({ message: " Update succesfull" });
}

export const putWeightTarget = async (req, res) => {

    const { participantId } = req.body
    const { weightTarget, targetDate, target } = req.body.data;

    const floatWeightTarget = parseFloat(weightTarget);
    const initDate = new Date();

    const selectedParticipant = await Participants.findByIdAndUpdate({ _id: participantId }, {
        weightTarget: { floatWeightTarget, targetDate, target, initDate },

    });

    if (!selectedParticipant) return res.status(404).json({ message: "No participant found" })
    res.status(200).json({ message: " Update succesfull" });
}

export const putTotalStars = async (req, res) => {

    const { participantId, totalStars } = req.body;
    const selectedParticipant = await Participants.findByIdAndUpdate({ _id: participantId }, {
        totalStars: totalStars
    });

    if (!selectedParticipant) return res.status(404).json({ message: "No participant found" })
    res.status(200).json({ message: " Update succesfull" });
}


export const putParticipantIcon = async (req, res) => {

    const { participantId, icon } = req.body;
    const selectedParticipant = await Participants.findByIdAndUpdate({ _id: participantId }, {
        icon: icon
    });

    if (!selectedParticipant) return res.status(404).json({ message: "No participant found" })
    res.status(200).json({ message: " Update succesfull" });
}

export const deleteParticipant = async (req, res) => {
    const { id } = req.params;

    const deletedParticipant = await Participants.findByIdAndDelete({ _id: id });
    if (!deletedParticipant) return res.status(404).json({ message: "No participant found" })

    await ParticipantsActivities.remove({ participantId: id });
    await ParticipantsIcons.remove({ participantId: id });

    res.status(200).json({ message: "Delete succesfull" });
}