import ParticipantsIcons from '../models/ParticipantsIcons';

export const getIcons = async (req, res) => {
    const { participantId } = req.params;
    const icons = await ParticipantsIcons.find({ participantId });
    if (!icons) return res.status(404).json({ message: "No icons found" })
    res.status(200).json({ icons })
}

export const unlockIcon = async (req, res) => {
    console.log(req.body)
    const { iconId, data } = req.body
    const icon = await ParticipantsIcons.findByIdAndUpdate({ _id: iconId }, {
        unlocked: data
    });
    if (!icon) return res.status(404).json({ message: "No icon found" })
    res.status(200).json({ icon })
}