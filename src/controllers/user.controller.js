import User from '../models/User';
import jwt from 'jsonwebtoken';
import config from '../config';

export const getUser = async (req, res) => {
   const { email } = req.params;

   const userFound = await User.findOne({ email: email }).populate("participants");

   res.status(200).json({ user: userFound });
}

export const getForgotUser = async (req, res) => {
   const { email } = req.params;
   const userFound = await User.findOne({ email: email })
   if (!userFound) return res.status(404).json({ message: "No user found" })

   res.status(200).json({ user: userFound });
}

export const updatePassword = async (req, res) => {
   const { email } = req.params;
   const { password, repeatPassword } = req.body.value;

   const updateUser = await User.findOneAndUpdate({ email: email }, {
      password: User.encryptPassword(password),
      confirmPassword: User.encryptPassword(repeatPassword),
   });

   if (!updateUser) return res.status(404).json({ message: "Can not update password" })

   const token = jwt.sign({ id: updateUser._id }, config.SECRET, {
      expiresIn: 172800000 // 48h
   });

   res.status(200).json({ token });
}