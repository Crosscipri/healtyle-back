import ParticipantsActivities from '../models/ParticipantsActivities';

export const getActivities = async (req, res) => {
    const { participantId } = req.params;
    const activities = await ParticipantsActivities.find({ participantId });
    if (!activities) return res.status(404).json({ message: "No activities found" })
    res.status(200).json({ activities })
}


export const putActivitie = async (req, res) => {
    const { activitieId } = req.body
    const initDate = new Date();
    const activitie = await ParticipantsActivities.findByIdAndUpdate({ _id: activitieId }, {
        initDate: initDate
    });
    if (!activitie) return res.status(404).json({ message: "No activitie found" })
    res.status(200).json({ activitie })
}

export const unlockActivitie = async (req, res) => {
    const { activitieId, data } = req.body
    const activitie = await ParticipantsActivities.findByIdAndUpdate({ _id: activitieId }, {
        unlocked: data
    });
    if (!activitie) return res.status(404).json({ message: "No activitie found" })
    res.status(200).json({ activitie })
}