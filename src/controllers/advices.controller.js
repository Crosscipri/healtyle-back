import advices from '../models/Advice';

export const getAdvices = async (req, res) => {
   const advice = await advices.aggregate(
    [ { $sample: { size: 1 } } ]
 )
   if (!advice) return res.status(404).json({ message: "No advice found" })

   res.status(200).json({advice})
}

