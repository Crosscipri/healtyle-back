import maleweight from '../models/MaleWeight';
import femaleweight from '../models/FemaleWeight';

export const getMaleWeight = async (req, res) => {
   const weight = await maleweight.find()
   if (!weight) return res.status(404).json({ message: "No male weight found" })

   res.status(200).json({weight})
}

export const getFemaleWeight = async (req, res) => {
   const weight = await femaleweight.find()
   if (!weight) return res.status(404).json({ message: "No female weight found" })

   res.status(200).json({weight})
}