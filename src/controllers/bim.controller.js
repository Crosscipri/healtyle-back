import malebim from '../models/MaleBim';
import femalebim from '../models/FemaleBim';

export const getMaleBim = async (req, res) => {
   const bim = await malebim.find()
   if (!bim) return res.status(404).json({ message: "No male bim found" })

   res.status(200).json({bim})
}

export const getFemaleBim = async (req, res) => {
   const bim = await femalebim.find()
   if (!bim) return res.status(404).json({ message: "No female bim found" })

   res.status(200).json({bim})
}