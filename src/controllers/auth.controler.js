import User from '../models/User';
import jwt from 'jsonwebtoken';
import config from '../config';

export const signUp = async (req, res) => {


    const { username, email, password, confirmPassword, hasTermsAndConditions } = req.body;


    const newUser = new User ({
        username: username,
        email: email,
        password: User.encryptPassword(password),
        confirmPassword:  User.encryptPassword(confirmPassword),
        hasTermsAndConditions: hasTermsAndConditions
    })
    
    const saveUser = await newUser.save();

    const token = jwt.sign({id: saveUser._id}, config.SECRET, {
        expiresIn: 172800000 // 48h
    });

    res.status(200).json({token});
}
export const signIn = async (req, res) => {
    const { loginUsername, loginPassword } = req.body;

    const userFound = await User.findOne({ email: loginUsername });
    if(!userFound) return res.status(400).json({message: "User not found"})

    const matchPassword = await User.comparePassword(loginPassword, userFound.password);
    if(!matchPassword) return res.status(400).json({token: null, message: "Invalid password"})

    const token = jwt.sign({id: userFound._id}, config.SECRET, {
        expiresIn: 172800000 // 24h
    });

    res.status(200).json({token})
}