import maleheight from '../models/MaleHeight';
import femaleheight from '../models/FemaleHeight';

export const getMaleHeight = async (req, res) => {
   const height = await maleheight.find()
   if (!height) return res.status(404).json({ message: "No male height found" })

   res.status(200).json({height})
}

export const getFemaleHeight = async (req, res) => {
   const height = await femaleheight.find()
   if (!height) return res.status(404).json({ message: "No female height found" })

   res.status(200).json({height})
}