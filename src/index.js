import app from './app';
import './database';

app.set('port', process.env.PORT || 4000)
app.listen(app.get('port'), () => {
    console.log(`conected to port ${app.get('port')}`)
});