import express from 'express';
import morgan from 'morgan';
import cors from 'cors'

import authRoutes from './routes/auth.routes';
import userRoutes from './routes/user.routes';
import participantsRoutes from './routes/participants.routes';
import weightRoutes from './routes/weight.routes';
import heightRoutes from './routes/height.routes';
import bimRoutes from './routes/bim.routes';
import advicesRoutes from './routes/advices.routes';
import activitiesRoutes from './routes/activities.routes';
import iconsRoutes from './routes/icons.routes';

const app = express();

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin,Content-Type,Accept,content-type,Authorization, Content-Length, X-Requested-With');
    next();
})



app.get('/', (req, res) => {
    return res.status(401).json({
        success: false,
        status: "Login Unsuccessful.",
        err: error,
    })
})

app.use('/auth', authRoutes);
app.use('/user', userRoutes);
app.use('/participants', participantsRoutes)
app.use('/weight', weightRoutes)
app.use('/height', heightRoutes)
app.use('/bim', bimRoutes)
app.use('/advices', advicesRoutes)
app.use('/activities', activitiesRoutes)
app.use('/icons', iconsRoutes)

export default app;

