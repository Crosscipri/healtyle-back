import { Router } from 'express';
const router = Router(); 

import * as iconsCtrl from '../controllers/icons.controller';

import { verifyToken } from '../middlewares';

router.get('/:participantId',verifyToken, iconsCtrl.getIcons);
router.put('/unlock',verifyToken, iconsCtrl.unlockIcon);

export default router;