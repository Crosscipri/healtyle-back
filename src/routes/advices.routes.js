import { Router } from 'express';
const router = Router(); 

import * as adviceCtrl from '../controllers/advices.controller';

import { verifyToken } from '../middlewares';

router.get('/',verifyToken, adviceCtrl.getAdvices);

export default router;