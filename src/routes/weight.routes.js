import { Router } from 'express';
const router = Router(); 

import * as userCtrl from '../controllers/weight.controller';

import { verifyToken } from '../middlewares';

router.get('/male',verifyToken, userCtrl.getMaleWeight);
router.get('/female',verifyToken, userCtrl.getFemaleWeight);

export default router;