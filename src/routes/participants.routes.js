import { Router } from 'express';
const router = Router(); 

import * as participantsCtrl from '../controllers/participants.controller';

import { verifyToken } from '../middlewares';

router.post('/', verifyToken, participantsCtrl.postParticipant);
router.get('/:userId',verifyToken, participantsCtrl.getParticipants);
router.get('/participant/:participantId',verifyToken, participantsCtrl.getParticipantById);
router.put('/selected', verifyToken, participantsCtrl.putSelectParticipant);
router.put('/', verifyToken, participantsCtrl.putParticipant);
router.put('/profileData', verifyToken, participantsCtrl.putProfileDataParticipant);
router.put('/weightTarget', verifyToken, participantsCtrl.putWeightTarget);
router.put('/totalStars', verifyToken, participantsCtrl.putTotalStars);
router.put('/icon', verifyToken, participantsCtrl.putParticipantIcon);
router.delete('/:id', verifyToken, participantsCtrl.deleteParticipant);

export default router;