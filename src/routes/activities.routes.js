import { Router } from 'express';
const router = Router(); 

import * as activitiesCtrl from '../controllers/activities.controller';

import { verifyToken } from '../middlewares';

router.get('/:participantId',verifyToken, activitiesCtrl.getActivities);
router.put('/activitie',verifyToken, activitiesCtrl.putActivitie);
router.put('/unlock',verifyToken, activitiesCtrl.unlockActivitie);

export default router;