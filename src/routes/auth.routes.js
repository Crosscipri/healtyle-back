import { Router } from 'express';
const router = Router();

import * as authCtrl from '../controllers/auth.controler';
import { checkDuplicateUsernameOrEmail } from '../middlewares';

router.post('/signup', checkDuplicateUsernameOrEmail, authCtrl.signUp)
router.post('/signin', authCtrl.signIn)

export default router;