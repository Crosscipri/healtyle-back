import { Router } from 'express';
const router = Router(); 

import * as userCtrl from '../controllers/height.controller';

import { verifyToken } from '../middlewares';

router.get('/male',verifyToken, userCtrl.getMaleHeight);
router.get('/female',verifyToken, userCtrl.getFemaleHeight);

export default router;