import { Router } from 'express';
const router = Router(); 

import * as userCtrl from '../controllers/user.controller';

import { verifyToken } from '../middlewares';

router.get('/login/:email',verifyToken, userCtrl.getUser)
router.get('/forgotUser/:email', userCtrl.getForgotUser)
router.put('/update/:email', userCtrl.updatePassword)

export default router;