import { Router } from 'express';
const router = Router(); 

import * as userCtrl from '../controllers/bim.controller';

import { verifyToken } from '../middlewares';

router.get('/male',verifyToken, userCtrl.getMaleBim);
router.get('/female',verifyToken, userCtrl.getFemaleBim);

export default router;