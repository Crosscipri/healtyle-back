import jwt from 'jsonwebtoken';
import config from '../config';

export function getId(req) {
    const token = req.headers["x-access-token"];
    if(!token) return

    const decoded = jwt.verify(token, config.SECRET);

    return decoded.id

}